<?php
 /**
 * @author Jeremy Michael Cerda <jcerda@topfloortech.com>
 *
 * Implements hook_views_default_views().
 *
 * Imports views from the /views directory.
 */

function views_repo_views_default_views() {
  $files = file_scan_directory(drupal_get_path('module', 'views_repo'). '/views', '/.view/');
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }
  if ($views) {
    return $views;
  }
}
